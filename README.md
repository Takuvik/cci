## cci

The `cci` R package allows to *crop* NetCDF file produced by the CCI that can be found in the this FTP ftp.rsg.pml.ac.uk. There is only one function at the moment called `crop_nc()`. 

The package can be installed using:

```r
if (!require("devtools")) install.packages("devtools")
devtools::install_git("https://PMassicotte@gitlab.com/Takuvik/cci.git"
```
